//
//  ShopTypeViewModelTest.swift
//  ProductSearchAppTests
//
//  Created by Burhan Dewantara on 11/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import XCTest
import RxSwift
@testable import ProductSearchApp

class ShopTypeViewModelTest: XCTestCase {
    
    var shopTypeViewParam:ShopTypeViewParam!
    var shopTypeViewModel:ShopTypeViewModel!
    let disposeBag = DisposeBag()
    
    override func setUp() {
        super.setUp()
        self.shopTypeViewParam = ShopTypeViewParam()
        self.shopTypeViewParam.isOfficial = true
        self.shopTypeViewParam.shopType = 100
        
        self.shopTypeViewModel =  ShopTypeViewModel(withShopTypeViewParam: self.shopTypeViewParam)
    }

    
    func testinit () {
        XCTAssertTrue(self.shopTypeViewModel.shopTypeViewParam.isOfficial!)
        XCTAssertEqual(self.shopTypeViewModel.shopTypeViewParam.shopType, 100)
    }
    
    
    func testResetButtonTapped() {
        let dismissExpectation = self.expectation(description: "should call event Dismiss")
        let applyFilterExpectation = self.expectation(description: "should call event applyFilter")
        
        self.shopTypeViewModel.rxEventApplyShopType
            .subscribe(onNext: { shopTypeViewParam in
                XCTAssertNil(shopTypeViewParam.shopType)
                applyFilterExpectation.fulfill()
            }).disposed(by: self.disposeBag)
        
        self.shopTypeViewModel.rxEventDismissView
            .subscribe(onNext: { _ in
                dismissExpectation.fulfill()
            }).disposed(by: self.disposeBag)
        
        self.shopTypeViewModel.resetButtonTapped()
        
        waitForExpectations(timeout: 0.1, handler: nil)
    }
    
    func testApplyFilter() {
        let dismissExpectation = self.expectation(description: "should call event Dismiss")
        let applyFilterExpectation = self.expectation(description: "should call event applyFilter")

        self.shopTypeViewModel.rxEventApplyShopType
            .subscribe(onNext: { shopTypeViewParam in
                XCTAssertEqual(shopTypeViewParam.shopType, 10)
                applyFilterExpectation.fulfill()
            }).disposed(by: self.disposeBag)

        self.shopTypeViewModel.rxEventDismissView
            .subscribe(onNext: { _ in
                dismissExpectation.fulfill()
            }).disposed(by: self.disposeBag)

        var newParam = ShopTypeViewParam()
        newParam.shopType = 10
        self.shopTypeViewModel.applyShopTypeFilter(newParam)

        waitForExpectations(timeout: 1, handler: nil)
    }
}
