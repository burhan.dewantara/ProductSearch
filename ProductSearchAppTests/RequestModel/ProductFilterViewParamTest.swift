//
//  ProductFilterViewParam.swift
//  ProductSearchAppTests
//
//  Created by Burhan Dewantara on 09/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import XCTest
@testable import ProductSearchApp

class ProductFilterViewParamTest: XCTestCase {
    
    func testInitDefault() {
        let productFilterViewParam = ProductFilterViewParam()
        
        XCTAssertEqual(productFilterViewParam.queryString, "samsung");
        XCTAssertNil(productFilterViewParam.maxPrice)
        XCTAssertNil(productFilterViewParam.minPrice)
        XCTAssertNil(productFilterViewParam.isWholesale)
        XCTAssertNil(productFilterViewParam.shopType)
        XCTAssertEqual(productFilterViewParam.start,0)
        XCTAssertEqual(productFilterViewParam.rows, 10);
    }
    
    func testInitWithParam() {
        var productFilterViewParam = ProductFilterViewParam()
        productFilterViewParam.queryString = "oppo"
        productFilterViewParam.isWholesale = true
        productFilterViewParam.isOfficial = true
        productFilterViewParam.maxPrice = 10000
        productFilterViewParam.minPrice = 100
        productFilterViewParam.shopType = 3
        productFilterViewParam.start = 3
        productFilterViewParam.rows = 30

        XCTAssertEqual(productFilterViewParam.queryString, "oppo");
        XCTAssertEqual(productFilterViewParam.maxPrice,10000)
        XCTAssertEqual(productFilterViewParam.minPrice,100)
        XCTAssertTrue(productFilterViewParam.isOfficial!)
        XCTAssertTrue(productFilterViewParam.isWholesale!)
        XCTAssertEqual(productFilterViewParam.shopType,3)
        XCTAssertEqual(productFilterViewParam.start,3)
        XCTAssertEqual(productFilterViewParam.rows,30)


    }
}
