//
//  Product+Utils.swift
//  ProductSearchAppTests
//
//  Created by Burhan Dewantara on 11/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation

@testable import ProductSearchApp
extension Product {
    static func random() -> Product {
        var product = Product()
        product.id = Int(arc4random())
        product.name = String.random()
        product.uri = String.random()
        product.imageUri = String.random()
        product.imageUriBig = String.random()
        product.price = String(Int(arc4random()))
        return product
    }
}

