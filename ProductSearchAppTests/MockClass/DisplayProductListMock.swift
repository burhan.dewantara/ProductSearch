//
//  DisplayProductListMock.swift
//  ProductSearchAppTests
//
//  Created by Burhan Dewantara on 11/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation
import RxSwift

@testable import ProductSearchApp
class DisplayProductListMock: DisplayProductListProtocol {
    
    var filterParam: ProductFilterViewParam?
    var stubbedProducts = [ProductViewParam]();
    
    func getProductList(withParam param: ProductFilterViewParam?) -> Observable<[ProductViewParam]?> {
        self.filterParam = param
        return Observable.just(self.stubbedProducts)
    }
}

extension DisplayProductListMock: SeederProtocol {
    func seed(_ count: Int) {
        for _ in 0 ..< count {
            let product = Product.random()
            let productViewParam = ProductViewParam(name: product.name, price: product.price, imageUri: product.imageUri)
            stubbedProducts.append(productViewParam)
        }
    }
}
