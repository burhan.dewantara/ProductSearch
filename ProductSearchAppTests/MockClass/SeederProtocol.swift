//
//  SeederProtocol.swift
//  ProductSearchAppTests
//
//  Created by Burhan Dewantara on 11/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation

protocol SeederProtocol {
    func seed(_ count:Int)
}
