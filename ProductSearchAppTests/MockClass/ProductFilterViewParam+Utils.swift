//
//  ProductFilterViewParam+Utils.swift
//  ProductSearchAppTests
//
//  Created by Burhan Dewantara on 11/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation
@testable import ProductSearchApp

extension ProductFilterViewParam {
    static func random() -> ProductFilterViewParam {
        var filterParam = ProductFilterViewParam()
        filterParam.isOfficial = false
        filterParam.isWholesale = false
        filterParam.minPrice = Int(arc4random() % 1000)
        filterParam.maxPrice = Int(arc4random() % 1000) + 1000
        filterParam.shopType = Int(arc4random() % 2)
        filterParam.queryString = String.random()
        
        return filterParam
    }

}
