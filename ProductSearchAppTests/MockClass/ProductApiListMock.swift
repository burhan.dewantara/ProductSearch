//
//  ProductApiListMock.swift
//  ProductSearchAppTests
//
//  Created by Burhan Dewantara on 09/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation
import RxSwift

@testable import ProductSearchApp

class ProductApiListMock: ProductListApiProtocol {
    
    var stubbedProducts = [Product]()
    var stubbedProductsWithParam = [Product]()
    
    func getProductWithParam(param: ProductQueryParam?) -> Observable<[Product]> {
        return Observable.just(stubbedProductsWithParam)
    }
    
    func getAllProduct() -> Observable<[Product]> {
        return Observable.just(stubbedProducts)
    }
}
extension ProductApiListMock: SeederProtocol {
    func seed(_ count: Int) {
        for _ in 0 ..< count {
            stubbedProducts.append(Product.random())
        }
        
        for _ in 0 ..< count {
            stubbedProductsWithParam.append(Product.random())
        }
    }
}
