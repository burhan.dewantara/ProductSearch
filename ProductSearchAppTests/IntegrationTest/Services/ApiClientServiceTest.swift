//
//  ApiClientServiceTest.swift
//  ProductSearchAppTests
//
//  Created by Burhan Dewantara on 08/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import XCTest
import RxBlocking
@testable import ProductSearchApp

class ApiClientServiceTest: XCTestCase {
    
    let apiClientService = ApiClientService()
    func testGetSuccess() {
        let response = try! self.apiClientService.get("search/v2.5/product", parameter: nil).toBlocking().first()!
        
        XCTAssertFalse(response.isEmpty)
        XCTAssertTrue(response.contains("OK"))
    }
    
    func testGetFailed() {
        XCTAssertThrowsError(try self.apiClientService.get("sembarangan", parameter: nil).toBlocking().first())
    }
    
    
}
