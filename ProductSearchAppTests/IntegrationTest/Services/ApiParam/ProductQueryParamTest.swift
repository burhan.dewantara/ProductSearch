//
//  ProductQueryParamTest.swift
//  ProductSearchAppTests
//
//  Created by Burhan Dewantara on 09/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import XCTest

@testable import ProductSearchApp

class ProductQueryParamTest: XCTestCase {
    
    func testToDictionaryWithAllParams() {
        var productQueryParam = ProductQueryParam()
        productQueryParam.queryString = "samsung"
        productQueryParam.minPrice = 100
        productQueryParam.maxPrice = 100000
        productQueryParam.wholeSale = true
        productQueryParam.official = true
        productQueryParam.fShop = 1
        productQueryParam.start = 11
        productQueryParam.rows = 100

        let queryDict = productQueryParam.toDictionary()

        XCTAssertEqual(queryDict.count,8)
        XCTAssertEqual(queryDict["q"] as? String,"samsung")
        XCTAssertEqual(queryDict["pmin"] as? Int,100)
        XCTAssertEqual(queryDict["pmax"] as? Int,100000)
        XCTAssertTrue(queryDict["wholesale"] as! Bool)
        XCTAssertTrue(queryDict["official"] as! Bool)
        XCTAssertEqual(queryDict["fshop"] as? Int,1)
        XCTAssertEqual(queryDict["start"] as? Int,11)
        XCTAssertEqual(queryDict["rows"] as? Int,100)

    }
    
    func testToDictionaryWithSomeParams() {
        var productQueryParam = ProductQueryParam()
        productQueryParam.queryString = "samsung"
        productQueryParam.minPrice = 100
        productQueryParam.maxPrice = 100000
     
        let queryDict = productQueryParam.toDictionary()

        XCTAssertEqual(queryDict.count,5)
        XCTAssertEqual(queryDict["q"] as? String,"samsung")
        XCTAssertEqual(queryDict["pmin"] as? Int,100)
        XCTAssertEqual(queryDict["pmax"] as? Int,100000)
        XCTAssertEqual(queryDict["start"] as? Int,0)
        XCTAssertEqual(queryDict["rows"] as? Int,10)
    }
    
    func testInitWithProductFilterParam() {
        var param = ProductFilterViewParam()
        param.isWholesale = true
        param.isOfficial = true
        param.shopType = 1
        param.maxPrice = 10000
        param.minPrice = 100
        param.queryString = "samsong"
        
        let productQueryParam = ProductQueryParam(fromProductFilterViewParam: param)
        
        XCTAssertTrue(productQueryParam.wholeSale!)
        XCTAssertTrue(productQueryParam.official!)
        XCTAssertEqual(productQueryParam.fShop,1)
        XCTAssertEqual(productQueryParam.minPrice,100)
        XCTAssertEqual(productQueryParam.maxPrice,10000)
        XCTAssertEqual(productQueryParam.queryString,"samsong")
    }
    
}
