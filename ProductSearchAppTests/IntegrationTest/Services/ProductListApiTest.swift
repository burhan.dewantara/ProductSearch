//
//  ProductListApiTest.swift
//  ProductSearchAppTests
//
//  Created by Burhan Dewantara on 08/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import XCTest
import RxBlocking

@testable import ProductSearchApp

class ProductListApiTest: XCTestCase {
    let productListApi = ProductListApi(apiClientService: ApiClientService())
    
    func testGetAllProducts() {
        let products = try! self.productListApi.getAllProduct().toBlocking().first()!
        XCTAssertGreaterThan(products.count, 0)
    }

    func testGetProductWithParam() {
        var param = ProductQueryParam()
        param.queryString = "samsung"
        param.start = 0
        param.rows = 2
        
        let products = try! self.productListApi.getProductWithParam(param: param).toBlocking().first()!
        XCTAssertEqual(products.count, 2)
    }
}
