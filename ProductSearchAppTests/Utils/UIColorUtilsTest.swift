//
//  UIColorUtilsTest.swift
//  ProductSearchAppTests
//
//  Created by Burhan Dewantara on 08/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import XCTest
import UIKit

@testable import ProductSearchApp

class UIColorUtilsTest: XCTestCase {
    
    func testUIColorInitColorFromHexString() {
        let uicolor = UIColor.colorFromHexString("#FF0000")
        let components = uicolor.cgColor.components!
        XCTAssertEqual(components[0], 1.0)
        XCTAssertEqual(components[1], 0.0)
        XCTAssertEqual(components[2], 0.0)
    }
    
}
