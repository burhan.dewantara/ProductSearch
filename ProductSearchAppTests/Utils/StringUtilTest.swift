//
//  StringUtilTest.swift
//  ProductSearchAppTests
//
//  Created by Burhan Dewantara on 09/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import XCTest

@testable import ProductSearchApp

class StringUtilTest: XCTestCase {
    
    func testRandomDefaultShouldReturnWithinLength () {
        let randomString = String.random()
        
        XCTAssertEqual(randomString.count, 10)
    }
    
    func testRandomShouldReturnWithinLength () {
        let randomString = String.random(len: 20)
        
        XCTAssertEqual(randomString.count, 20)
    }
}
