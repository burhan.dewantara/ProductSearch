//
//  ShopResponseTest.swift
//  ProductSearchAppTests
//
//  Created by Burhan Dewantara on 07/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import XCTest
@testable import ProductSearchApp

class ShopResponseTest: XCTestCase {
    
    func testInit() {
        let jsonString = """
        {
            "id": 112233,
            "name": "Kuanto_Acc",
            "uri": "https://www.tokopedia.com/kuantoacc",
            "is_gold": 1,
            "rating": null,
            "location": "Kota Bekasi",
            "reputation_image_uri": "https://inbox.tokopedia.com/reputation/v1/badge/shop/1286377",
            "shop_lucky": "https://clover.tokopedia.com/badges/merchant/v1?shop_id=1286377",
            "city": "Kota Bekasi"
        }
        """
        
        let jsonData = jsonString.data(using: .utf8)
        let response = try! JSONDecoder().decode(ShopResponse.self, from: jsonData!)
        
        XCTAssertEqual(response.id, 112233)
        XCTAssertEqual(response.name, "Kuanto_Acc")
        XCTAssertEqual(response.uri, "https://www.tokopedia.com/kuantoacc")
        XCTAssertTrue(response.isGold)
        XCTAssertNil(response.rating)
        XCTAssertEqual(response.location, "Kota Bekasi")
        XCTAssertEqual(response.reputationImageUri, "https://inbox.tokopedia.com/reputation/v1/badge/shop/1286377")
        XCTAssertEqual(response.shopLucky, "https://clover.tokopedia.com/badges/merchant/v1?shop_id=1286377")
        XCTAssertEqual(response.city, "Kota Bekasi")
        
    }

}
