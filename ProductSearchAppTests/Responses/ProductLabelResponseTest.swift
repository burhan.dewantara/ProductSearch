//
//  ProductLabelResponseTest.swift
//  ProductSearchAppTests
//
//  Created by Burhan Dewantara on 07/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import XCTest
@testable import ProductSearchApp

class ProductLabelResponseTest: XCTestCase {
    
    func testInit() {
        let jsonString = """
        {
            "title": "title",
            "color": "blue",
        }
        """
        
        let jsonData = jsonString.data(using: .utf8)
        let response = try! JSONDecoder().decode(ProductLabelResponse.self, from: jsonData!)
        
        XCTAssertEqual(response.title, "title")
        XCTAssertEqual(response.color, "blue")
    }
}
