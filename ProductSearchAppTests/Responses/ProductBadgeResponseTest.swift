//
//  ProductBadgeResponseTest.swift
//  ProductSearchAppTests
//
//  Created by Burhan Dewantara on 07/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import XCTest
@testable import ProductSearchApp

class ProductBadgeResponseTest: XCTestCase {
    
    func testInit() {
        let jsonString = """
        {
            "title": "title",
            "image_url": "https://www.apple.com",
        }
        """
        
        let jsonData = jsonString.data(using: .utf8)
        let response = try! JSONDecoder().decode(ProductBadgeResponse.self, from: jsonData!)
        
        XCTAssertEqual(response.title, "title")
        XCTAssertEqual(response.image_url, "https://www.apple.com")
    }
}

