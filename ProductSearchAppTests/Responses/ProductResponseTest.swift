//
//  ProductResponseTest.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 07/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import XCTest
@testable import ProductSearchApp

class ProductResponseTest: XCTestCase {
    
    func testInit() {
        let jsonString = """
        {
            "id": 51792832,
            "name": "BUMPER MIRROR CASE SAMSUNG GRAND 2 / G7106",
            "uri": "https://www.tokopedia.com/kuantoacc/bumper-mirror-case-samsung-grand-2-g7106?trkid=f%3DCa0000L000P1W1S1Sh00Co0Po0Fr0Cb0_page%3D1_ob%3D32_q%3Dsamsung_catid%3D69_po%3D6",
            "image_uri": "https://ecs7.tokopedia.net/img/cache/200-square/product-1/2016/7/19/1286377/1286377_a150f384-061e-4d42-8f69-aef8963f87c5.jpg",
            "image_uri_700": "https://ecs7.tokopedia.net/img/cache/700/product-1/2016/7/19/1286377/1286377_a150f384-061e-4d42-8f69-aef8963f87c5.jpg",
            "price": "Rp 20.000",
            "shop": {
                "id": 1286377,
                "name": "Kuanto_Acc",
                "uri": "https://www.tokopedia.com/kuantoacc",
                "is_gold": 1,
                "rating": null,
                "location": "Kota Bekasi",
                "reputation_image_uri": "https://inbox.tokopedia.com/reputation/v1/badge/shop/1286377",
                "shop_lucky": "https://clover.tokopedia.com/badges/merchant/v1?shop_id=1286377",
                "city": "Kota Bekasi"
            },
            "wholesale_price": [
                {
                    "count_min": 3,
                    "count_max": 5,
                    "price": "Rp 18.000"
                }
            ],
            "condition": 1,
            "preorder": 0,
            "department_id": 69,
            "rating": 66,
            "is_featured": 0,
            "count_review": 9,
            "count_talk": 0,
            "count_sold": 0,
            "labels": [
                {
                    "title": "Grosir",
                    "color": "#ffffff"
                }
            ],
            "badges": [
                {
                    "title": "Gold Merchant",
                    "image_url": "https://ecs7.tokopedia.net/img/gold-active-large.png"
                }
            ],
            "original_price": "Rp 0",
            "discount_expired": "",
            "discount_percentage": 0,
            "stock": 0
        }
        """
        let jsonData = jsonString.data(using: .utf8)
        let _ = try! JSONDecoder().decode(ProductResponse.self, from: jsonData!)
        
    }
    
    
}
