//
//  ShopTypeViewParamTest.swift
//  ProductSearchAppTests
//
//  Created by Burhan Dewantara on 11/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import XCTest

@testable import ProductSearchApp
class ShopTypeViewParamTest: XCTestCase {
   
    func testInitWithParam() {
        var viewParam = ShopTypeViewParam()
        viewParam.isOfficial = true
        viewParam.shopType = 3
        
        XCTAssertTrue(viewParam.isOfficial!)
        XCTAssertEqual(viewParam.shopType,3)
    }
    
}
