//
//  ProductCategoryResponseTest.swift
//  ProductSearchAppTests
//
//  Created by Burhan Dewantara on 08/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import XCTest
@testable import ProductSearchApp

class ProductCategoryResponseTest: XCTestCase {
    
    func testInit() {
        let jsonString = """
        {
            "id": 39,
            "name": "Aksesoris Motor",
            "total_data": "2",
            "parent_id": 63,
            "child_id": [
                1306,
                1309
            ],
            "level": 2
        }
        """
        
        let jsonData = jsonString.data(using: .utf8)
        let response = try! JSONDecoder().decode(ProductCategoryResponse.self, from: jsonData!)
        
        XCTAssertEqual(response.id, 39)
        XCTAssertEqual(response.name, "Aksesoris Motor")
        XCTAssertEqual(response.totalData, "2")
        XCTAssertEqual(response.childId?.count, 2)
        XCTAssertEqual(response.parentId, 63)
        XCTAssertEqual(response.level, 2)
    }
}
