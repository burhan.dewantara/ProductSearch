//
//  ProductCategoryListResponseTest.swift
//  ProductSearchAppTests
//
//  Created by Burhan Dewantara on 08/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import XCTest
@testable import ProductSearchApp

class ProductCategoryListResponseTest: XCTestCase {
    
    func testInit() {
        let jsonString = """
        {
           "data": {
                "16": {
                    "id": 16,
                    "name": "Mainan & Hobi Lainnya",
                    "total_data": "4",
                    "parent_id": 55,
                    "child_id": [
                        505,
                        545
                    ],
                    "level": 2
                },
                "24": {
                    "id": 24,
                    "name": "Handphone",
                    "total_data": "48",
                    "parent_id": 65,
                    "child_id": null,
                    "level": 2
                }
            },
            "selected_id": "123",
        }
        """
        
        let jsonData = jsonString.data(using: .utf8)
        let response = try! JSONDecoder().decode(ProductCategoryListResponse.self, from: jsonData!)
        
        XCTAssertEqual(response.data?.count, 2)
        XCTAssertEqual(response.selectedId, "123")
        
    }

}
