//
//  WholeSalePriceResponseTest.swift
//  ProductSearchAppTests
//
//  Created by Burhan Dewantara on 07/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import XCTest
@testable import ProductSearchApp

class WholeSalePriceResponseTest: XCTestCase {
    
    func testInit() {
        let jsonString = """
        {
            "count_min": 3,
            "count_max": 100,
            "price": "2000",
        }
        """
        
        let jsonData = jsonString.data(using: .utf8)
        let response = try! JSONDecoder().decode(WholeSalePriceResponse.self, from: jsonData!)
        
        XCTAssertEqual(response.count_min, 3)
        XCTAssertEqual(response.count_max, 100)
        XCTAssertEqual(response.price, "2000")
    }
}
