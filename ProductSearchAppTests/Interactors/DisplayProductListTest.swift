//
//  DisplayProductListTest.swift
//  ProductSearchAppTests
//
//  Created by Burhan Dewantara on 08/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import XCTest
import RxBlocking

@testable import ProductSearchApp

class DisplayProductListTest: XCTestCase {

    func testGetProduct() {
        let productApiList = ProductApiListMock()
        productApiList.seed(2)
        
        let displayProductList = DisplayProductList(productListApi: productApiList)
 
        let productViewParams = try! displayProductList.getProductList().toBlocking().first()!
        
        XCTAssertEqual(productViewParams?.count, 2)
        XCTAssertEqual(productViewParams?[0].name, productApiList.stubbedProductsWithParam[0].name)
        XCTAssertEqual(productViewParams?[1].name, productApiList.stubbedProductsWithParam[1].name)
        XCTAssertEqual(productViewParams?[0].imageUri, productApiList.stubbedProductsWithParam[0].imageUri)
    }
    
    //MARK: -
    
    
}
