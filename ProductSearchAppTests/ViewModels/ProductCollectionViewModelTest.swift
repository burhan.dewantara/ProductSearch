//
//  ProductCollectionViewModelTest.swift
//  ProductSearchAppTests
//
//  Created by Burhan Dewantara on 10/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import XCTest

@testable import ProductSearchApp

class ProductCollectionViewModelTest: XCTestCase {

    func testInit() {
        let mockedName = String.random()
        let mockedPrice = String.random()
        let mockedImageUri = "https://image.com"
        
        let param = ProductViewParam(name:mockedName,
                                     price: mockedPrice,
                                     imageUri: mockedImageUri)
        
        let vm = ProductCollectionViewModel(withProductViewParam: param)
        
        XCTAssertEqual(vm.name, mockedName)
        XCTAssertEqual(vm.price, mockedPrice)
        XCTAssertEqual(vm.imageUri?.absoluteString, mockedImageUri)
    }
    
}
