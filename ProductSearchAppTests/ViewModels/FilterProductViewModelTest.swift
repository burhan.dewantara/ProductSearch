//
//  FilterProductViewModelTest.swift
//  ProductSearchAppTests
//
//  Created by Burhan Dewantara on 11/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import XCTest
import RxSwift

@testable import ProductSearchApp

class FilterProductViewModelTest: XCTestCase {
    var filterParam:ProductFilterViewParam!
    var filterProductViewModel:FilterProductViewModel!
    let disposeBag = DisposeBag()

    override func setUp() {
        super.setUp()
        self.filterParam = ProductFilterViewParam.random()
    	self.filterProductViewModel = FilterProductViewModel(withFilterViewParam: filterParam)
    }

    func testInit() {
        XCTAssertEqual(filterProductViewModel.productFilterViewParam.queryString, filterParam.queryString)
    }
    
    func testShopTypeButtonTapped() {
        let expectation = self.expectation(description: "should call event Show Shop type")
        
        self.filterProductViewModel.rxEventShowShopTypeTagView
            .subscribe(onNext: { _ in
                expectation.fulfill()
            }).disposed(by: self.disposeBag)
        
        self.filterProductViewModel.shopTypeButtonTapped()
        
        waitForExpectations(timeout: 0.1, handler: nil)
    }
        
    func testResetButtonTapped() {
        let dismissExpectation = self.expectation(description: "should call event Dismiss")
        let applyFilterExpectation = self.expectation(description: "should call event applyFilter")
        
        self.filterProductViewModel.rxEventApplyProductFilter
            .subscribe(onNext: { filterViewParam in
                XCTAssertNil(filterViewParam.maxPrice)
                applyFilterExpectation.fulfill()
            }).disposed(by: self.disposeBag)
        
        self.filterProductViewModel.rxEventDismissView
            .subscribe(onNext: { _ in
                dismissExpectation.fulfill()
            }).disposed(by: self.disposeBag)
        
        self.filterProductViewModel.resetButtonTapped()
        
        waitForExpectations(timeout: 0.1, handler: nil)
    }
    
    func testApplyFilter() {
        let dismissExpectation = self.expectation(description: "should call event Dismiss")
        let applyFilterExpectation = self.expectation(description: "should call event applyFilter")
        
        self.filterProductViewModel.rxEventApplyProductFilter
            .subscribe(onNext: { filterViewParam in
            XCTAssertEqual(filterViewParam.maxPrice, 10)
            applyFilterExpectation.fulfill()
        }).disposed(by: self.disposeBag)
        
        self.filterProductViewModel.rxEventDismissView
            .subscribe(onNext: { _ in
                dismissExpectation.fulfill()
            }).disposed(by: self.disposeBag)
        
        var newFilterParam = ProductFilterViewParam()
        newFilterParam.maxPrice = 10
        self.filterProductViewModel.applyFilter(newFilterParam)
        
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func testCreateShopTypeViewModel () {
        let vm = self.filterProductViewModel.createShopTypeViewModel()
        XCTAssertEqual(vm.shopTypeViewParam.isOfficial, self.filterParam.isOfficial)
        XCTAssertEqual(vm.shopTypeViewParam.shopType, self.filterParam.shopType)
    }
    
    func testCreateShopTypeTagViewModel_WhenIsOfficialAndGoldMerchant() {
        self.filterProductViewModel.productFilterViewParam.isOfficial = true
        self.filterProductViewModel.productFilterViewParam.shopType = 2
        let vm1 = self.filterProductViewModel.createShopTypeTagCellViewModel(0)
        let vm2 = self.filterProductViewModel.createShopTypeTagCellViewModel(1)
        XCTAssertEqual(self.filterProductViewModel.shopTypeTags.count, 2)
        XCTAssertEqual(vm1!.shopType, ShopType.officialStore)
        XCTAssertEqual(vm2!.shopType, ShopType.goldMerchant)
    }
    
    func testCreateShopTypeTagViewModel_WhenIsOfficialOnly() {
        self.filterProductViewModel.productFilterViewParam.isOfficial = true
        self.filterProductViewModel.productFilterViewParam.shopType = 1
        let vm = self.filterProductViewModel.createShopTypeTagCellViewModel(0)
        XCTAssertEqual(self.filterProductViewModel.shopTypeTags.count, 1)
        XCTAssertEqual(vm!.shopType, ShopType.officialStore)
    }

    func testCreateShopTypeTagViewModel_WhenGoldMerchantOnly() {
        self.filterProductViewModel.productFilterViewParam.isOfficial = false
        self.filterProductViewModel.productFilterViewParam.shopType = 2
        let vm = self.filterProductViewModel.createShopTypeTagCellViewModel(0)
        XCTAssertEqual(self.filterProductViewModel.shopTypeTags.count, 1)
        XCTAssertEqual(vm!.shopType, ShopType.goldMerchant)
    }
    
    func testCreateShopTypeTagViewModel_WhenNoShopTypeSelected() {
        self.filterProductViewModel.productFilterViewParam.isOfficial = nil
        self.filterProductViewModel.productFilterViewParam.shopType = nil
        let vm = self.filterProductViewModel.createShopTypeTagCellViewModel(0)
        XCTAssertEqual(self.filterProductViewModel.shopTypeTags.count, 0)
        XCTAssertNil(vm)
    }


    func testRemoveShopTypeTag() {
        self.filterProductViewModel.productFilterViewParam.isOfficial = true
        self.filterProductViewModel.productFilterViewParam.shopType = 2
        
        XCTAssertEqual(self.filterProductViewModel.shopTypeTags.count, 2)
        self.filterProductViewModel.removeShopTypeTag(tag: .officialStore)
        XCTAssertEqual(self.filterProductViewModel.shopTypeTags.count, 1)
        self.filterProductViewModel.removeShopTypeTag(tag: .goldMerchant)
        XCTAssertEqual(self.filterProductViewModel.shopTypeTags.count, 0)
        
    }
}
