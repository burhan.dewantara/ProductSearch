//
//  SearchViewModelTest.swift
//  ProductSearchAppTests
//
//  Created by Burhan Dewantara on 11/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import XCTest

@testable import ProductSearchApp

class SearchViewModelTest: XCTestCase {
    var displayProductListMock : DisplayProductListMock!
    var searchViewModel : SearchViewModel!
    
    override func setUp() {
        super.setUp()
        self.displayProductListMock = DisplayProductListMock()
        self.searchViewModel = SearchViewModel(displayProductList: self.displayProductListMock)
    }
    
    func testViewDidLoad() {
        self.displayProductListMock.seed(2)
        self.searchViewModel.viewDidLoad()
        
        XCTAssertNotNil(self.displayProductListMock.filterParam)
        XCTAssertEqual(self.displayProductListMock.filterParam?.start,0)
        XCTAssertEqual(self.displayProductListMock.filterParam?.rows,10)
    }
    
    func testLoadMoreProduct() {
        self.displayProductListMock.seed(20)
        self.searchViewModel.loadMoreProduct()
        
        XCTAssertNotNil(self.displayProductListMock.filterParam)
        XCTAssertEqual(self.displayProductListMock.filterParam?.start,10)
        XCTAssertEqual(self.displayProductListMock.filterParam?.rows,10)
        XCTAssertEqual(self.displayProductListMock.stubbedProducts[10].name,
                       self.searchViewModel.products.value[10].name)
 
    }
    
    func testCreateProductCellViewModel() {
        let param = ProductViewParam.init(name: "name", price: "price", imageUri: "uri")
        let vm = self.searchViewModel.createProductCellViewModel(withParam: param)
        
        XCTAssertEqual(vm.name, "name")
        XCTAssertEqual(vm.price, "price")
        XCTAssertEqual(vm.imageUri?.absoluteString, "uri")
    }
    
    func testCreateFilterProductViewModel() {
        self.searchViewModel.filterParam.maxPrice = 10000
        self.searchViewModel.filterParam.minPrice = 10
        let vm = self.searchViewModel.createFilterProductViewModel()
        self.searchViewModel.filterParam.minPrice = 1020
        
        XCTAssertEqual(vm.productFilterViewParam.maxPrice, 10000)
        XCTAssertEqual(vm.productFilterViewParam.minPrice, 10)
    }
    
}
