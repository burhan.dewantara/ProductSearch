//
//  ProductViewParamTest.swift
//  ProductSearchAppTests
//
//  Created by Burhan Dewantara on 09/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import XCTest

@testable import ProductSearchApp

class ProductViewParamTest: XCTestCase {
    
    func testInit() {
        let productViewParam = ProductViewParam(name: "name",
                                                price: "price",
                                                imageUri: "uri")
        
        XCTAssertEqual(productViewParam.name, "name")
        XCTAssertEqual(productViewParam.price, "price")
        XCTAssertEqual(productViewParam.imageUri, "uri")
    }
}
