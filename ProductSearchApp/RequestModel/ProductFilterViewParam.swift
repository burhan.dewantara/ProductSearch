//
//  ProductFilterViewParam.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 09/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation

struct ProductFilterViewParam {
    var queryString = "samsung"
    var minPrice:Int?
    var maxPrice:Int?
    var isWholesale:Bool?
    var isOfficial:Bool?
    var shopType:Int?
    var start = 0
    var rows = 10
    
}
