//
//  ShopTypeViewParam.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 11/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation

struct ShopTypeViewParam {
    var isOfficial:Bool?
    var shopType:Int?
}

