//
//  ProductCollectionViewCell.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 10/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import UIKit
import Kingfisher

class ProductCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet weak var productContainerView: UIView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    
    var productCollectionViewModel : ProductCollectionViewModel!
    
    class func getContainerHeightBasedOn(width:CGFloat) -> CGFloat {
        return 28 / 20 * width
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        productImageView.image = nil
        productNameLabel.text = nil
        productPriceLabel.text = nil
    }
    
    func viewDidLoad() {
        self.productNameLabel.text = productCollectionViewModel.name
        self.productPriceLabel.text = productCollectionViewModel.price
        self.productImageView.kf.setImage(with: productCollectionViewModel.imageUri)
    }
    
}
