//
//  ProductCollectionViewModel.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 10/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation

class ProductCollectionViewModel {
    
    var imageUri:URL?
    var name:String
    var price:String
    
    init(withProductViewParam param:ProductViewParam){
        self.imageUri = URL(string: param.imageUri)
        self.name = param.name
        self.price = param.price
    }
    
    
}
