//
//  ShopTypeTagCollectionViewModel.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 11/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation
import RxSwift

class ShopTypeTagCollectionViewModel {
    
    var shopType:ShopType

    let eventRemoveTag = PublishSubject<ShopType>()
    var rxEventRemoveTag: Observable<ShopType> {
        get {
            return self.eventRemoveTag
        }
    }

    init(withShopType shopType:ShopType) {
        self.shopType = shopType
    }
    
    
    func removeButtonTapped () {
        self.eventRemoveTag.onNext(self.shopType)
    }
}
