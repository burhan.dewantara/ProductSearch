//
//  ShopTypeTagCollectionViewCell.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 11/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import UIKit
import RxSwift

class ShopTypeTagCollectionViewCell : UICollectionViewCell {
 
    var shopTypeTagViewModel:ShopTypeTagCollectionViewModel!
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var shopTypeLabel: UILabel!
    
    let eventRemoveTag = PublishSubject<ShopType>()
    var rxEventRemoveTag :Observable<ShopType> {
        get {
            return self.eventRemoveTag
        }
    }
    
    func viewDidLoad(){
        self.setupUI()
        self.setupViewModel()
    }
    
    func setupViewModel(){
    
        self.shopTypeTagViewModel.rxEventRemoveTag
            .subscribe(self.eventRemoveTag)
            .disposed(by: self.disposeBag)

        self.shopTypeLabel.text = self.shopTypeTagViewModel.shopType.rawValue
    
    }
    
    @IBAction func onRemoveButtonTapped(_ sender: Any) {
        self.shopTypeTagViewModel.removeButtonTapped()
    }
    
    private func setupUI(){
        self.container.layer.cornerRadius = 15
        self.container.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
        self.container.layer.borderWidth = 0.5
        
        self.removeButton.layer.cornerRadius = 15
        self.removeButton.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
        self.removeButton.layer.borderWidth = 0.5
        
        
    }
}
