//
//  FilterProductViewController.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 11/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RangeSeekSlider

class FilterProductViewController : UIViewController {
    
    var filterProductViewModel: FilterProductViewModel!
    let disposeBag = DisposeBag()

    @IBOutlet weak var shopTypeCollectionView: UICollectionView!
    @IBOutlet weak var wholesaleSwitch: UISwitch!
    @IBOutlet weak var priceRangeSlider: CustomRangeSeekSlider!

    let eventApplyFilter = PublishSubject<ProductFilterViewParam>()
    var rxEventApplyFilter: Observable<ProductFilterViewParam> {
        get {
            return self.eventApplyFilter
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.setupUIValue()
        self.setupViewModel()
        self.registerNib()
        self.shopTypeCollectionView.delegate = self
        self.shopTypeCollectionView.dataSource = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "openShopTypeView"){
            if let viewController = segue.destination as? ShopTypePageViewController{
                viewController.shopTypeViewModel = self.filterProductViewModel.createShopTypeViewModel()
                viewController.rxEventApplyShopTypeParam.subscribe(onNext: { [weak self] shopTypeViewParam in
                    guard let weakSelf = self else { return }
                    weakSelf.filterProductViewModel.applyShopTypeViewParam(shopTypeViewParam)
                    weakSelf.shopTypeCollectionView.reloadData()
                }).disposed(by: self.disposeBag)
            }
            
        }
    }
    
    //MARK: actions -
    
    @IBAction func onResetButtonTapped(_ sender: Any) {
        self.filterProductViewModel.resetButtonTapped()
    }
    
    @IBAction func onShopTypeButtonTapped(_ sender: Any) {
        self.filterProductViewModel.shopTypeButtonTapped()
    }
    
    @IBAction func onApplyButtonTapped(_ sender: Any) {
        var param = self.filterProductViewModel.productFilterViewParam
        param.start = 0
        param.maxPrice = Int(self.priceRangeSlider.selectedMaxValue)
        param.minPrice = Int(self.priceRangeSlider.selectedMinValue)
        param.isWholesale = self.wholesaleSwitch.isOn
        self.filterProductViewModel.applyFilter(param)
    }
    
    //MARK: -
    private func setupUIValue() {
        if let maxPrice = self.filterProductViewModel.productFilterViewParam.maxPrice {
            self.priceRangeSlider.selectedMaxValue = CGFloat(maxPrice)
        }
        
        if let minPrice = self.filterProductViewModel.productFilterViewParam.minPrice {
            self.priceRangeSlider.selectedMinValue = CGFloat(minPrice)
        }
        
        self.wholesaleSwitch.isOn = false
        if let isWholesale = self.filterProductViewModel.productFilterViewParam.isWholesale {
            self.wholesaleSwitch.isOn = isWholesale
        }
    }
    
    private func setupViewModel() {
        self.filterProductViewModel.rxEventDismissView
            .subscribe(onNext: { [weak self] in
                guard let weakSelf = self else { return }
                weakSelf.navigationController?.popViewController(animated: true)
            }).disposed(by: self.disposeBag)

        self.filterProductViewModel.rxEventApplyProductFilter
            .subscribe(self.eventApplyFilter)
            .disposed(by: self.disposeBag)
        
        self.filterProductViewModel.rxEventShowShopTypeTagView
            .subscribe(onNext: { [weak self] _ in
                guard let weakSelf = self else { return }
                weakSelf.performSegue(withIdentifier: "openShopTypeView", sender: nil)
            }).disposed(by: self.disposeBag)
    }

    fileprivate func registerNib()
    {
        let nib = UINib(nibName: String(describing:ShopTypeTagCollectionViewCell.self), bundle: nil)
        self.shopTypeCollectionView.register(nib, forCellWithReuseIdentifier: String(describing:ShopTypeTagCollectionViewCell.self))
    }

}

extension FilterProductViewController: UICollectionViewDelegate {
    
}

extension FilterProductViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.filterProductViewModel.shopTypeTags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing:ShopTypeTagCollectionViewCell.self),
                                                         for: indexPath) as? ShopTypeTagCollectionViewCell{
            cell.shopTypeTagViewModel = self.filterProductViewModel.createShopTypeTagCellViewModel(indexPath.row)
            cell.rxEventRemoveTag.subscribe(onNext: { [weak self] shopTypeTag in
                guard let weakSelf = self else { return }
                weakSelf.filterProductViewModel.removeShopTypeTag(tag:shopTypeTag)
                weakSelf.shopTypeCollectionView.reloadData()
                
            }).disposed(by: self.disposeBag)
            cell.viewDidLoad()
            
            return cell
        }
        
        return UICollectionViewCell()
        
        
    }
    
}
