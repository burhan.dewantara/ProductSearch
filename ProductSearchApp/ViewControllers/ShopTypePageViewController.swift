//
//  ShopTypePageViewController.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 11/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import UIKit
import RxSwift

class ShopTypePageViewController: UIViewController {
    var shopTypeViewModel : ShopTypeViewModel!
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var goldMerchantSwitch: UISwitch!
    @IBOutlet weak var officialStoreSwitch: UISwitch!
    
    
    let eventApplyShopTypeParam = PublishSubject<ShopTypeViewParam>()
    var rxEventApplyShopTypeParam: Observable<ShopTypeViewParam> {
        get {
            return self.eventApplyShopTypeParam
        }
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupViewModel()
        self.setupUI()
    }
    
    @IBAction func onResetButtonTapped(_ sender: Any) {
        self.shopTypeViewModel.resetButtonTapped()
    }
    
    @IBAction func onApplyButtonTapped(_ sender: Any) {
        var param = ShopTypeViewParam()
        param.shopType = self.goldMerchantSwitch.isOn ? 2 : nil

        param.isOfficial = self.officialStoreSwitch.isOn
        if param.shopType == nil {
            param.shopType = self.officialStoreSwitch.isOn ? 1 : nil
        }
        
        self.shopTypeViewModel.applyShopTypeFilter(param)
    }
    
    //MARK: -
    private func setupViewModel() {
        self.shopTypeViewModel.rxEventDismissView
            .subscribe(onNext: { [weak self] _ in
                guard let weakSelf = self else { return }
                weakSelf.navigationController?.popViewController(animated: true)
            }).disposed(by: self.disposeBag)
        
        self.shopTypeViewModel.rxEventApplyShopType
            .subscribe(self.eventApplyShopTypeParam)
            .disposed(by: self.disposeBag)

    }
    
    private func setupUI() {
        if let shopType = self.shopTypeViewModel.shopTypeViewParam.shopType {
            self.goldMerchantSwitch.isOn = shopType == 2
        }
        
        if let isOfficial = self.shopTypeViewModel.shopTypeViewParam.isOfficial {
            self.officialStoreSwitch.isOn = isOfficial
        }
    }
}
