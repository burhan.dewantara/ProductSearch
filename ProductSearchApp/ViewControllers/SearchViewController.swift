//
//  SearchViewController.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 07/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SearchViewController: UIViewController {
    @IBOutlet weak var productCollectionView: UICollectionView!
    
    var searchViewModel:SearchViewModel!
    fileprivate let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.productCollectionView.dataSource = self
        self.productCollectionView.delegate = self
        self.registerNib()
        
        self.setupViewModel()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "openFilterView") {
            let filterProductViewController = segue.destination as! FilterProductViewController
            filterProductViewController.filterProductViewModel = self.searchViewModel.createFilterProductViewModel()
            filterProductViewController.rxEventApplyFilter.subscribe(onNext: { [weak self] productFilterViewParam in
                guard let weakSelf = self else { return }
                weakSelf.productCollectionView.contentOffset.y = 0
                weakSelf.searchViewModel.filterParam = productFilterViewParam
                weakSelf.searchViewModel.loadProduct(reset: true)
            }).disposed(by: self.disposeBag)
        }
    }
    
    //MARK: actions -
    @IBAction func onFilterButtonTapped(_ sender: Any) {
        self.searchViewModel.filterButtonTapped()
    }

    //MARK: -
    fileprivate func setupViewModel() {
        self.searchViewModel.products.asDriver()
            .drive(onNext: { [weak self] _ in
                guard let weakSelf = self else { return }
                weakSelf.productCollectionView.reloadData()
            }).disposed(by: self.disposeBag)
        
        self.searchViewModel.rxEventShowFilterView
            .subscribe(onNext: { [weak self] in
                guard let weakSelf = self else { return }
                weakSelf.performSegue(withIdentifier: "openFilterView", sender: nil)
                }
            ).disposed(by: self.disposeBag)
        
        self.searchViewModel.viewDidLoad()
    }
    
    fileprivate func registerNib()
    {
        let nib = UINib(nibName: String(describing:ProductCollectionViewCell.self), bundle: nil)
        self.productCollectionView.register(nib, forCellWithReuseIdentifier: String(describing:ProductCollectionViewCell.self))
    }
    
}

extension SearchViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize = UIScreen.main.bounds.size
        let width = (screenSize.width / 2)
        let height = ProductCollectionViewCell.getContainerHeightBasedOn(width: width)
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension SearchViewController: UICollectionViewDelegate {

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
        if (bottomEdge >= scrollView.contentSize.height) {
            self.searchViewModel.loadMoreProduct()
        }
    }
}

extension SearchViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.searchViewModel.products.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing:ProductCollectionViewCell.self), for: indexPath) as! ProductCollectionViewCell
        cell.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor
        cell.layer.borderWidth = 0.5
        
        let currentProductViewParam = self.searchViewModel.products.value[indexPath.row]
        cell.productCollectionViewModel = self.searchViewModel.createProductCellViewModel(withParam: currentProductViewParam)
        cell.viewDidLoad()
        return cell
    }
}

