//
//  SwinjectStoryboard+Setup.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 08/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation
import Swinject
import SwinjectStoryboard

extension SwinjectStoryboard {
    
    @objc class func setup() {
        
        registerServices()
        registerInteractors()
        registerViewModels()
        registerViewControllers()
    }
    
    class func registerViewControllers() {

        defaultContainer.storyboardInitCompleted(ShopTypePageViewController.self) { r, c in
            c.shopTypeViewModel = r.resolve(ShopTypeViewModel.self)!
        }

        defaultContainer.storyboardInitCompleted(FilterProductViewController.self) { r, c in
            c.filterProductViewModel = r.resolve(FilterProductViewModel.self)!
        }
        
        defaultContainer.storyboardInitCompleted(SearchViewController.self) { r, c in
            c.searchViewModel = r.resolve(SearchViewModel.self)!
        }
    }
    
    class func registerViewModels() {
        
        defaultContainer.register(SearchViewModel.self) { r in
            SearchViewModel(displayProductList: r.resolve(DisplayProductListProtocol.self)!)
        }
        
        defaultContainer.register(FilterProductViewModel.self) { r in
            FilterProductViewModel(withFilterViewParam: ProductFilterViewParam())
        }

        defaultContainer.register(ShopTypeViewModel.self) { r in
            ShopTypeViewModel(withShopTypeViewParam: ShopTypeViewParam())
        }

    }
    
    class func registerInteractors() {
        defaultContainer.register(DisplayProductListProtocol.self) { r in
            DisplayProductList(productListApi: r.resolve(ProductListApiProtocol.self)!)
        }
    }
    
    class func registerServices() {
        defaultContainer.register(ProductListApiProtocol.self) { r in
            ProductListApi(apiClientService: r.resolve(ApiClientServiceProtocol.self)!)
        }
        
        defaultContainer.register(ApiClientServiceProtocol.self) { _ in ApiClientService() }
    }
    
}
