//
//  ProductLabel.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 08/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation
import UIKit

struct ProductLabel {
    var title = ""
    var color = UIColor.clear
}
