//
//  Shop.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 08/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation

struct  Shop {
    var id = 0
    var name = ""
    var uri = ""
    var isGold = false
    var rating:Int?
    var location = ""
    var reputationImageUri = ""
    var shopLucky = ""
    var city = ""
    
    
}
