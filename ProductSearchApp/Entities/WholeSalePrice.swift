//
//  WholeSalePrice.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 08/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation

struct  WholeSalePrice {
    var count_min = 0
    var count_max = 0
    var price = ""
}
