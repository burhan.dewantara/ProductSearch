//
//  Product.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 08/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation

struct Product {
    var id = 0
    var name = ""
    var uri = ""
    var imageUri = ""
    var imageUriBig = ""
    var price = ""
    var shop:Shop?
    var wholesalePrice:[WholeSalePrice]?
    
    var condition = 0
    var preorder = 0
    var departmentId = 0
    var rating = 0
    var isFeatured = false
    var countReview = 0
    var countTalk = 0
    var countSold = 0
    
    var labels:[ProductLabel]?
    var badges:[ProductBadge]?
    var originalPrice = ""
    var discountExpired = ""
    var discountPercentage = 0
    var stock = 0
    
    
}
