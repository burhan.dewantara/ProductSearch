//
//  HeaderResponse.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 08/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation

struct HeaderResponse {
    var totalData:Int?
    var totalDataNoCategory:Int?
    var additionalParams:String?
    var processTime:Float?
    var suggestionInstead:Int?
    
    enum CodingKeys : String, CodingKey {
        case totalData = "total_data"
        case totalDataNoCategory = "total_data_no_category"
        case additionalParams = "additional_params"
        case processTime = "process_time"
        case suggestionInstead = "suggestion_instead"
        
    }
}

extension HeaderResponse : Decodable {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        totalData = try container.decodeIfPresent(Int.self, forKey: .totalData);
        totalDataNoCategory = try container.decodeIfPresent(Int.self, forKey: .totalDataNoCategory);
        additionalParams = try container.decodeIfPresent(String.self, forKey: .additionalParams);
        processTime = try container.decodeIfPresent(Float.self, forKey: .processTime);
        suggestionInstead = try container.decodeIfPresent(Int.self, forKey: .suggestionInstead);
    }
}

extension HeaderResponse : Encodable {
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(totalData, forKey: .totalData)
        try container.encode(totalDataNoCategory, forKey: .totalDataNoCategory)
        try container.encode(additionalParams, forKey: .additionalParams)
        try container.encode(processTime, forKey: .processTime)
        try container.encode(suggestionInstead, forKey: .suggestionInstead)
        
    }
}
