//
//  ProductResponse.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 07/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation

struct ProductResponse {
    var id:Int
    var name:String
    var uri:String
    var imageUri:String
    var imageUriBig:String
    var price:String
    var shop:ShopResponse
    var wholesalePrice:[WholeSalePriceResponse]?
    var condition:Int
    var preorder:Int
    var departmentId:Int
    var rating:Int?
    var isFeatured:Bool
    var countReview:Int
    var countTalk:Int
    var countSold:Int
    var labels:[ProductLabelResponse]?
    var badges:[ProductBadgeResponse]?
    var originalPrice:String
    var discountExpired:String
    var discountPercentage:Int
    var stock:Int
    
    enum CodingKeys : String, CodingKey {
        case id
        case name
        case uri
        case imageUri = "image_uri"
        case imageUriBig = "image_uri_700"
        case price
        case shop
        case wholesalePrice = "wholesale_price"
        case condition
        case preorder
        case departmentId = "department_id"
        case rating
        case isFeatured = "is_featured"
        case countReview = "count_review"
        case countTalk = "count_talk"
        case countSold = "count_sold"
        case labels
        case badges
        case originalPrice = "original_price"
        case discountExpired = "discount_expired"
        case discountPercentage = "discount_percentage"
        case stock
    }
    
    func toProduct () -> Product {
        var product = Product()
        product.id = self.id
        product.name = self.name
        product.uri = self.uri
        product.imageUri = self.imageUri
        product.imageUriBig = self.imageUriBig
        product.price = self.price
        product.shop = self.shop.toShop()
        product.wholesalePrice = self.wholesalePrice?.map { $0.toWholeSalePrice() }
        product.condition = self.condition
        product.preorder = self.preorder
        product.departmentId = self.departmentId
        product.isFeatured = self.isFeatured
        product.countReview = self.countReview
        product.countTalk = self.countTalk
        product.countSold = self.countSold
        product.labels = self.labels?.map { $0.toProductLabel() }
        product.badges = self.badges?.map { $0.toProductBadge() }
        product.originalPrice = self.originalPrice
        product.discountExpired = self.discountExpired
        product.discountPercentage = self.discountPercentage
        product.stock = self.stock
        return product
    }
}

extension ProductResponse: Decodable {
    init(from decoder: Decoder) throws {
        let container = try! decoder.container(keyedBy: CodingKeys.self)
        id = try! container.decode(Int.self, forKey: .id)
        name = try! container.decode(String.self, forKey: .name)
        uri = try! container.decode(String.self, forKey: .uri)
        imageUri = try! container.decode(String.self, forKey: .imageUri)
        imageUriBig = try! container.decode(String.self, forKey: .imageUriBig)
        price = try! container.decode(String.self, forKey: .price)
        shop = try! container.decode(ShopResponse.self, forKey: .shop)
        wholesalePrice = try! container.decodeIfPresent([WholeSalePriceResponse].self, forKey: .wholesalePrice)
        condition = try! container.decode(Int.self, forKey: .condition)
        preorder = try! container.decode(Int.self, forKey: .preorder)
        departmentId = try! container.decode(Int.self, forKey: .departmentId)
        rating = try! container.decodeIfPresent(Int.self, forKey: .rating)
        isFeatured = Bool(truncating: try! container.decode(Int.self, forKey: .isFeatured) as NSNumber)
        countReview = try! container.decode(Int.self, forKey: .countReview)
        countSold = try! container.decode(Int.self, forKey: .countSold)
        countTalk = try! container.decode(Int.self, forKey: .countTalk)
        labels = try! container.decodeIfPresent([ProductLabelResponse].self, forKey: .labels)
        badges = try! container.decodeIfPresent([ProductBadgeResponse].self, forKey: .badges)
        originalPrice = try! container.decode(String.self, forKey: .originalPrice)
        discountExpired = try! container.decode(String.self, forKey: .discountExpired)
        discountPercentage = try! container.decode(Int.self, forKey: .discountPercentage)
        stock = try! container.decode(Int.self, forKey: .stock)
    }
}

extension ProductResponse: Encodable {
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try! container.encode(id, forKey: .id)
        try! container.encode(name, forKey: .name)
        try! container.encode(uri, forKey: .uri)
        try! container.encode(imageUri, forKey: .imageUri)
        try! container.encode(imageUriBig, forKey: .imageUriBig)
        try! container.encode(price, forKey: .price)
        try! container.encode(shop, forKey: .shop)
        try! container.encode(wholesalePrice, forKey: .wholesalePrice)
        try! container.encode(condition, forKey: .condition)
        try! container.encode(preorder, forKey: .preorder)
        try! container.encode(departmentId, forKey: .departmentId)
        try! container.encode(rating, forKey: .rating)
        try! container.encode(isFeatured, forKey: .isFeatured)
        try! container.encode(countReview, forKey: .countReview)
        try! container.encode(countSold, forKey: .countSold)
        try! container.encode(countTalk, forKey: .countTalk)
        try! container.encode(labels, forKey: .labels)
        try! container.encode(badges, forKey: .badges)
        try! container.encode(originalPrice, forKey: .originalPrice)
        try! container.encode(discountExpired, forKey: .discountExpired)
        try! container.encode(discountPercentage, forKey: .discountPercentage)
        try! container.encode(stock, forKey: .stock)
    }
}


