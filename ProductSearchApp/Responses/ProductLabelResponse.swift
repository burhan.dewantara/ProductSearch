//
//  ProductLabelResponse.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 07/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation
import UIKit

struct ProductLabelResponse : Codable {
    var title:String
    var color:String
    
    func toProductLabel() -> ProductLabel {
        var productLabel = ProductLabel()
        productLabel.title = self.title
        productLabel.color = UIColor.colorFromHexString(self.color)
        return productLabel
    }
}
