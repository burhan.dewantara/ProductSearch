//
//  StatusResponse.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 08/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation

struct StatusResponse :Codable {
    var error_code:Int
    var message:String
}
