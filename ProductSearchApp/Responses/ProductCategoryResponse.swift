//
//  ProductCategoryResponse.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 08/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation

struct ProductCategoryResponse {
    var id:Int
    var name:String
    var totalData:String
    var parentId:Int
    var childId:[Int]?
    var level:Int
    
    enum CodingKeys : String, CodingKey {
        case id
        case name
        case totalData = "total_data"
        case parentId = "parent_id"
        case childId = "child_id"
        case level
    }
}

extension ProductCategoryResponse : Decodable {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decode(Int.self, forKey: .id);
        name = try container.decode(String.self, forKey: .name);
        totalData = try container.decode(String.self, forKey: .totalData);
        parentId = try container.decode(Int.self, forKey: .parentId);
        childId = try container.decodeIfPresent([Int].self, forKey: .childId)
        level = try container.decode(Int.self, forKey: .level);
    }
}

extension ProductCategoryResponse : Encodable {
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(name, forKey: .name)
        try container.encode(totalData, forKey: .totalData)
        try container.encode(parentId, forKey: .parentId)
        try container.encode(childId, forKey: .childId)
        try container.encode(level, forKey: .level)
        
    }
}
