//
//  ShopResponse.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 07/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation

struct ShopResponse  {
    var id:Int
    var name:String
    var uri:String
    var isGold:Bool
    var rating:Int?
    var location:String
    var reputationImageUri:String
    var shopLucky:String
    var city:String

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case uri
        case isGold = "is_gold"
        case rating
        case location
        case reputationImageUri = "reputation_image_uri"
        case shopLucky = "shop_lucky"
        case city
    }
    
    func toShop() -> Shop {
        var shop = Shop()
        shop.id = self.id
        shop.name = self.name
        shop.uri = self.uri
        shop.isGold = self.isGold
        shop.rating = self.rating ?? 0
        shop.location = self.location
        shop.reputationImageUri = self.reputationImageUri
        shop.shopLucky = self.shopLucky
        shop.city = self.city
        return shop
    }
}

extension ShopResponse: Decodable {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        uri = try container.decode(String.self, forKey: .uri)
        isGold = Bool(truncating: try container.decode(Int.self, forKey: .isGold) as NSNumber)
        rating = try container.decodeIfPresent(Int.self, forKey: .rating)
        location = try container.decode(String.self, forKey: .location)
        reputationImageUri = try container.decode(String.self, forKey: .reputationImageUri)
        shopLucky = try container.decode(String.self, forKey: .shopLucky)
        city = try container.decode(String.self, forKey: .city)
    }
}

extension ShopResponse : Encodable {
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(name, forKey: .name)
        try container.encode(uri, forKey: .uri)
        try container.encode(isGold, forKey: .isGold)
        try container.encode(rating, forKey: .rating)
        try container.encode(location, forKey: .location)
        try container.encode(reputationImageUri, forKey: .reputationImageUri)
        try container.encode(shopLucky, forKey: .shopLucky)
        try container.encode(city, forKey: .city)
    }
}
