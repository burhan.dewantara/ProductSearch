//
//  ProductListResponse
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 07/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation

struct ProductListResponse : Codable {
    var status: StatusResponse
    var header: HeaderResponse
    var category: ProductCategoryListResponse
    var data: [ProductResponse]
}
