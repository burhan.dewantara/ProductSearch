//
//  ProductCategoryListResponse.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 08/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation

struct ProductCategoryListResponse {
    var data:[String:ProductCategoryResponse]?
    var selectedId:String
    
    enum CodingKeys : String, CodingKey {
        case data
        case selectedId = "selected_id"
    }
}

extension ProductCategoryListResponse : Decodable {
    init(from decoder: Decoder) throws {
        let container = try! decoder.container(keyedBy: CodingKeys.self)
        data = try! container.decodeIfPresent([String:ProductCategoryResponse].self, forKey: .data)
        selectedId = try! container.decode(String.self, forKey: .selectedId)
    }
}

extension ProductCategoryListResponse : Encodable {
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try! container.encodeIfPresent(data, forKey: .data)
        try! container.encode(selectedId, forKey: .selectedId)

    }
}

