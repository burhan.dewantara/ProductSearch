//
//  WholeSalePriceResponse.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 07/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation

struct WholeSalePriceResponse : Codable {
    var count_min:Int
    var count_max:Int
    var price:String
    
    func toWholeSalePrice () -> WholeSalePrice {
        var wholeSalePrice = WholeSalePrice ()
        wholeSalePrice.count_max = self.count_max
        wholeSalePrice.count_min = self.count_min
        wholeSalePrice.price = self.price
        return wholeSalePrice;
    }
}
