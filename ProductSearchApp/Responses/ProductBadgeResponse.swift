//
//  ProductBadgeResponse.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 07/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation

struct ProductBadgeResponse : Codable {
    var title:String
    var image_url:String
    
    func toProductBadge () -> ProductBadge {
        var productBadge = ProductBadge()
        productBadge.title = self.title
        productBadge.imageUrl = self.image_url
        return productBadge
    }
}

