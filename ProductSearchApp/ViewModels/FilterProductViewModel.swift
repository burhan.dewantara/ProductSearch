//
//  FilterProductViewModel.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 11/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation
import RxSwift

public enum ShopType:String {
    case officialStore = "Official Store"
    case goldMerchant = "Gold Merchant"
}


class FilterProductViewModel : NSObject {

    var productFilterViewParam : ProductFilterViewParam
    var shopTypeTags: [ShopType] {
        get {
            var tags = [ShopType]()
            if let isOfficial = self.productFilterViewParam.isOfficial {
                if isOfficial {
                    tags.append(ShopType.officialStore)
                }
            }
            
            if let shopType = self.productFilterViewParam.shopType {
                if shopType == 2 {
                    tags.append(ShopType.goldMerchant)
                }
            }
            return tags
        }
        
    }

    //MARK: Events -
    let eventShowShopTypeTagView = PublishSubject<Void>()
    var rxEventShowShopTypeTagView: Observable<Void> {
        get {
            return self.eventShowShopTypeTagView
        }
    }
    let eventDismissView = PublishSubject<Void>()
    var rxEventDismissView: Observable<Void> {
        get {
            return self.eventDismissView
        }
    }

    let eventApplyProductFilter = PublishSubject<ProductFilterViewParam>()
    var rxEventApplyProductFilter: Observable<ProductFilterViewParam> {
        get {
            return self.eventApplyProductFilter
        }
    }

    //MARK: -
    func createShopTypeTagCellViewModel(_ index:Int) -> ShopTypeTagCollectionViewModel? {
        if index < self.shopTypeTags.count{
            return ShopTypeTagCollectionViewModel(withShopType: self.shopTypeTags[index])
        } else {
            return nil
        }
    }
    
    func createShopTypeViewModel() -> ShopTypeViewModel {
        var shopTypeViewParam = ShopTypeViewParam()
        shopTypeViewParam.isOfficial = self.productFilterViewParam.isOfficial
        shopTypeViewParam.shopType = self.productFilterViewParam.shopType
        return ShopTypeViewModel(withShopTypeViewParam: shopTypeViewParam)
    }
    
    //MARK: -
    init(withFilterViewParam param:ProductFilterViewParam) {
        self.productFilterViewParam = param
    }
    
    func applyFilter (_ param:ProductFilterViewParam) {
        self.eventApplyProductFilter.onNext(param)
        self.eventDismissView.onNext(())
    }
    
    func resetButtonTapped () {
        self.eventApplyProductFilter.onNext(ProductFilterViewParam())
        self.eventDismissView.onNext(())
    }
    
    func shopTypeButtonTapped () {
        self.eventShowShopTypeTagView.onNext(())
    }
    
    func applyShopTypeViewParam(_ param:ShopTypeViewParam){
        self.productFilterViewParam.isOfficial = param.isOfficial
        self.productFilterViewParam.shopType = param.shopType
    }

    func removeShopTypeTag(tag :ShopType) {
        switch tag {
        case .goldMerchant:
            if let isOfficial = self.productFilterViewParam.isOfficial {
                self.productFilterViewParam.shopType = isOfficial ? 1 : nil
            } else {
                self.productFilterViewParam.shopType = nil
            }

        case .officialStore:
            if let shopType = self.productFilterViewParam.shopType {
                self.productFilterViewParam.shopType = (shopType == 2) ? 2 : nil
                self.productFilterViewParam.isOfficial = nil
            } else {
                self.productFilterViewParam.shopType = nil
                self.productFilterViewParam.isOfficial = nil
            }
        }
    }
}
