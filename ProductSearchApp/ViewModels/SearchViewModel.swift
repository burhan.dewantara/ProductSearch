//
//  SearchViewModel.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 10/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class SearchViewModel : NSObject {
    var products = Variable<[ProductViewParam]>([ProductViewParam]())
    var filterParam = ProductFilterViewParam()
    
    private let displayProductList:DisplayProductListProtocol
    private var isLoadingContent = false

    private var eventShowFilterView = PublishSubject<Void>()
    var rxEventShowFilterView: Observable<Void> {
        get{
            return self.eventShowFilterView
        }
    }
    
    init(displayProductList: DisplayProductListProtocol) {
        self.displayProductList = displayProductList
    }
    
    func viewDidLoad() {
        loadProduct(reset:true)
    }
    
    func loadMoreProduct(){
        if(isLoadingContent) {
            return
        }

        self.filterParam.start += self.filterParam.rows
        loadProduct(reset:false)
    }
    
    
    func loadProduct(reset:Bool) {
        self.isLoadingContent = true
        _ = self.displayProductList.getProductList(withParam: self.filterParam)
            .takeUntil(self.rx.deallocated)
            .subscribe(onNext: { [weak self] productViewParams in
                guard let weakSelf = self else { return }
                
                if let products = productViewParams {
                    if(reset) {
                        weakSelf.products.value.removeAll()
                    }
                    weakSelf.products.value.append(contentsOf:products)
                }
                weakSelf.isLoadingContent = false
            })
    }
    
    func filterButtonTapped () {
        self.eventShowFilterView.onNext(())
    }
    
    //MARK: -
    func createProductCellViewModel(withParam param:ProductViewParam) -> ProductCollectionViewModel {
        return ProductCollectionViewModel(withProductViewParam: param)
    }

    func createFilterProductViewModel() -> FilterProductViewModel {
        return FilterProductViewModel(withFilterViewParam: self.filterParam)
    }


}
