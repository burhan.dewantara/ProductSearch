//
//  ShopTypeViewModel.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 11/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation
import RxSwift

class ShopTypeViewModel {
    
    var shopTypeViewParam:ShopTypeViewParam!

    let eventApplyShopType = PublishSubject<ShopTypeViewParam>()
    var rxEventApplyShopType: Observable<ShopTypeViewParam> {
        get {
            return self.eventApplyShopType
        }
    }
    
    let eventDismissView = PublishSubject<Void>()
    var rxEventDismissView: Observable<Void> {
        get {
            return self.eventDismissView
        }
    }
    
    init(withShopTypeViewParam param:ShopTypeViewParam) {
        self.shopTypeViewParam = param
    }
    
    func resetButtonTapped () {
        self.eventApplyShopType.onNext(ShopTypeViewParam())
        self.eventDismissView.onNext(())
    }
    
    func applyShopTypeFilter (_ param:ShopTypeViewParam) {
        self.eventApplyShopType.onNext(param)
        self.eventDismissView.onNext(())
    }
}
