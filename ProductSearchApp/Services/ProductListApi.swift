//
//  ProductListApi.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 08/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation
import RxSwift

protocol ProductListApiProtocol {
    func getAllProduct() -> Observable<[Product]>
    func getProductWithParam(param:ProductQueryParam?) -> Observable<[Product]>
}

class ProductListApi : ProductListApiProtocol {
    let apiClientService : ApiClientServiceProtocol
    let endPoint = "search/v2.5/product"
    
    init(apiClientService: ApiClientServiceProtocol) {
        self.apiClientService = apiClientService
    }

    func getProductWithParam(param: ProductQueryParam?) -> Observable<[Product]> {
        return self.apiClientService.get(endPoint, parameter: param?.toDictionary())
            .map { response in
                guard let jsonData = response.data(using: .utf8) else { return [Product]() }
                let productListResponse = try! JSONDecoder().decode(ProductListResponse.self, from:jsonData);
                
                return productListResponse.data.flatMap({ response -> Product in
                    return response.toProduct()
                })
        };

    }
    
    func getAllProduct() -> Observable<[Product]> {
        return self.getProductWithParam(param: nil)
    }
}
