//
//  ApiClientService.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 08/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

enum HTTPErrorCodeType : Error {
    case pageNotFound
}

protocol ApiClientServiceProtocol {
    func get(_ path: String) -> Observable<String>;
    func get(_ path: String, parameter:[String:Any]?) -> Observable<String>
}

class ApiClientService: ApiClientServiceProtocol {

    fileprivate let host = "https://ace.tokopedia.com/"
    
    func get(_ path: String) -> Observable<String> {
        return self.get(path, parameter: nil)
    }
    
    func get(_ path: String, parameter:[String:Any]?) -> Observable<String> {

        return Observable.create({ [weak self] observer -> Disposable in
            guard let weakSelf = self else { return Disposables.create {} }
            let fullPath = weakSelf .createURL(path)
        
            let request = Alamofire.request(fullPath,
                                           method: .get,
                                           parameters: parameter,
                                           encoding: URLEncoding.default,
                                           headers: nil)
                .responseString { response in
                    if let error = response.result.error {
                        observer.onError(error);
                    } else {
                        if response.response?.statusCode != 200 {
                            observer.onError(HTTPErrorCodeType.pageNotFound)
                        } else {
                            if let responseString = response.result.value {
                                observer.onNext(responseString)
                                observer.onCompleted()
                            }
                        }
                    }
            }
            
            return Disposables.create {
             request.cancel()
            };
        })
    }
    
    
    private func createURL(_ path: String) -> String {
        return self.host + path
    }
    
}
