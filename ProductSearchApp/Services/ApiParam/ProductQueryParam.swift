//
//  ProductQueryParam.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 09/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation

struct ProductQueryParam {
    var queryString:String?
    var minPrice:Int?
    var maxPrice:Int?
    var wholeSale:Bool?
    var official:Bool?
    var fShop:Int?
    var start:Int?
    var rows:Int?

    init() {
       
    }
    
    init (fromProductFilterViewParam param:ProductFilterViewParam) {
        queryString = param.queryString
        minPrice = param.minPrice
        maxPrice = param.maxPrice
        wholeSale = param.isWholesale
        official = param.isOfficial
        fShop = param.shopType
        start = param.start
        rows = param.rows
    }
    
    func toDictionary() -> [String:Any] {
        var dictionary = [String:Any]()
        
        if let queryString = self.queryString {
            dictionary["q"] = queryString
        }
        
        if let minPrice = self.minPrice {
            dictionary["pmin"] = minPrice
        }

        if let maxPrice = self.maxPrice {
            dictionary["pmax"] = maxPrice
        }

        if let wholesale = self.wholeSale {
            dictionary["wholesale"] = wholesale
        }

        if let official = self.official {
            dictionary["official"] = official
        }

        if let fshop = self.fShop {
            dictionary["fshop"] = fshop
        }

        dictionary["start"] = 0
        if let start = self.start {
            dictionary["start"] = start
        }
        
        dictionary["rows"] = 10
        if let rows = self.rows {
            dictionary["rows"] = rows
        }
        
        return dictionary
    }
    
}

