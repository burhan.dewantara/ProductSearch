//
//  ProductViewParam.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 09/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation

struct ProductViewParam {
    var name:String
    var price:String
    var imageUri:String
    
    init(name:String, price:String, imageUri:String) {
        self.name = name
        self.price = price
        self.imageUri = imageUri
    }
}
