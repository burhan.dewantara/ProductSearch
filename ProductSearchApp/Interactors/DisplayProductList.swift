//
//  DisplayProductList.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 08/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation
import RxSwift

protocol DisplayProductListProtocol {
    func getProductList(withParam param:ProductFilterViewParam?) -> Observable<[ProductViewParam]?>
}

class DisplayProductList : DisplayProductListProtocol {
    private let productListApi: ProductListApiProtocol
    
    init(productListApi: ProductListApiProtocol) {
        self.productListApi = productListApi;
    }
    
    func getProductList(withParam param:ProductFilterViewParam? = nil) -> Observable<[ProductViewParam]?> {
        var productQueryParam = ProductQueryParam()
        if let filterParam = param {
            productQueryParam = ProductQueryParam(fromProductFilterViewParam: filterParam)
        }
        
        return self.productListApi.getProductWithParam(param: productQueryParam)
            .map { products in
                return products.map { ProductViewParam.init(name: $0.name, price: $0.price, imageUri: $0.imageUri) }
        }
    }
}
