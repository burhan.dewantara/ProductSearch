//
//  CustomRangeSeekSlider.swift
//  ProductSearchApp
//
//  Created by Burhan Dewantara on 11/03/18.
//  Copyright © 2018 NCxT. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RangeSeekSlider

class CustomRangeSeekSlider: RangeSeekSlider {

    override func setupStyle() {
        numberFormatter.positivePrefix = "Rp "
    }
}

extension CustomRangeSeekSlider {
    public var rxMaxValue: AnyObserver<CGFloat> {
        return Binder.init(self, binding: { (view, maxValue) in
            view.maxValue = maxValue
        }).asObserver()
    }

}
